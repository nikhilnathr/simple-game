# Simple Game

A simple ball blast game made using Javascript with Canvas. The app is a progressive web app and made to work fully offline.

Live demo at [https://ball-blast.netlify.app](https://ball-blast.netlify.app).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To get a copy of this project just clone this repository by typing the code.

```bash
git clone git@gitlab.com:nikhilnathr/simple-game.git
```

### Installing

Install a new web-browser link Chrome or Firefox on your machine

Just open the **index.html** using the web-browser and you can see the game in action

To view the code just open **index.html**, **script.js**, **loading.js** and **style.css** using any text editor and you are good to go

## Structure

The files are in the typical structure.

- index.html - Contains the basic layout of the game. All other scripts are called from this file.

- style.css - A CSS file contains only some simple styles to make the game and controls look good.

- loading.js - File which removes the loading screen after the document has been loaded.

- script.js - This is where the fun happens. All the game contents and functions are in this file.

## Built With

- HTML
- CSS
- JavaScript

## License

This project is licensed under the MIT License - see the LICENSE.md file for details

## Acknowledgments

This game was started based on Ball Blast a game in android. But as it was very hard to implement all physics functions without any libraries, this project was made to be very simple.

A big thanks to [StackOverflow](https://stackoverflow.com) and its contiributers
