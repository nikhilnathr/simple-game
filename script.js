let screenHeight = window.innerHeight,
  screenWidth = window.innerWidth;

const STATE = {
  MENU: "MENU",
  PLAYING: "PLAYING",
  GAME_OVER: "GAME_OVER",
  PAUSE: "PAUSE",
};
let GAME_STATE = STATE.MENU;

const menuControls = document.getElementById("menu-controls");
const pauseControls = document.getElementById("pause-menu-controls");
const gameControls = document.getElementById("game-controls");
const endControls = document.getElementById("end-controls");

const controlShoot = document.getElementById("control-shoot");
const controlLeft = document.getElementById("control-left");
const controlRight = document.getElementById("control-right");

let canvas = document.getElementById("myCanvas"),
  ctx = canvas.getContext("2d"),
  lastCalledTime,
  fps, //to calculate fps
  animationID,
  gameRunning = false, //to pause/resume the animation
  leftPressed = false,
  rightPressed = false,
  ctrlPressed = false;

let cartBody,
  cartWheel1,
  cartWheel2,
  cartX,
  dCartX = 0,
  bulletArray = [],
  polygonArray = [],
  polygonColors = [
    "#c80059",
    "#A9C800",
    "#00AFC8",
    "#C81900",
    "#7C00C8",
    "#00C835",
  ];

let platformHeight = 100,
  bulletRadius = 10,
  bulletMinInterval = 0.15,
  lastBulletTime = 0,
  maxPolygonSize = 60,
  minPolygonSize = 20,
  minPolygonSide = 15,
  maxPolygonSide = 20,
  lastPolygonTime = 0,
  polygonMinInterval = 5,
  polygonYDefault = 70,
  polygonLeftDefault = -maxPolygonSize,
  polygonDefaultVx = 1,
  polygonDefaultVy = 0,
  accelerationY = 0.04,
  totalPolygonCount = 0,
  points = 0;

if (screenWidth > screenHeight) {
  canvas.height = screenHeight - 40;
  canvas.width = (screenHeight * 9) / 16;
} else {
  canvas.width = screenWidth;
  canvas.height = (screenWidth * 16) / 9 - 40;
}
document.addEventListener("keydown", keyDownHandler, false);
document.addEventListener("keyup", keyUpHandler, false);

controlShoot.addEventListener(
  "touchstart",
  () => {
    controlHandler(KEY.Z, true);
  },
  false
);

controlShoot.addEventListener(
  "touchend",
  () => {
    controlHandler(KEY.Z, false);
  },
  false
);

controlLeft.addEventListener(
  "touchstart",
  () => {
    controlHandler(KEY.LEFT, true);
  },
  false
);

controlLeft.addEventListener(
  "touchend",
  () => {
    controlHandler(KEY.LEFT, false);
  },
  false
);

controlRight.addEventListener(
  "touchstart",
  () => {
    controlHandler(KEY.RIGHT, true);
  },
  false
);

controlRight.addEventListener(
  "touchend",
  () => {
    controlHandler(KEY.RIGHT, false);
  },
  false
);

const KEY = {
  Z: "KeyZ",
  RIGHT: "ArrowRight",
  LEFT: "ArrowLeft",
};

function controlHandler(key, state) {
  if (key === KEY.Z) {
    ctrlPressed = state;
  } else if (key === KEY.LEFT) {
    leftPressed = state;
  } else if (key === KEY.RIGHT) {
    rightPressed = state;
  }
}

function keyDownHandler(e) {
  if (e.code === "Space") {
    if (GAME_STATE == STATE.MENU || GAME_STATE == STATE.PAUSE) {
      startGame();
    } else if (GAME_STATE == STATE.PLAYING) {
      pauseGame();
    }
  }

  const entry = Object.entries(KEY).find((ee) => ee[1] === e.code);
  if (entry) {
    controlHandler(entry[1], true);
  }
}

function keyUpHandler(e) {
  const entry = Object.entries(KEY).find((ee) => ee[1] === e.code);
  if (entry) {
    controlHandler(entry[1], false);
  }
}
//canvas.addEventListener("touchmove", mouseMoveHandler, false);
//function mouseMoveHandler(e) {
//    let relativeX = e ;
//    if(gameRunning) {
//        dCartX = cartX - relativeX.changedTouches[0].clientX;
//        cartX = relativeX.changedTouches[0].clientX;
//    }
//}

function initializeComponents() {
  cartBody = new Image();
  cartBody.src = "./vechicle.svg";

  cartWheel1 = new Image();
  cartWheel1.src = "./cart-wheel.svg";

  cartWheel2 = new Image();
  cartWheel2.src = "./cart-wheel.svg";
}
cartX = canvas.width / 2;

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  if (
    rightPressed &&
    cartX < canvas.width - cartBody.width - cartWheel2.width / 2 - 4
  ) {
    dCartX += 4;
    cartX += 4;
  } else if (leftPressed && cartX > cartWheel2.width / 2 + 4) {
    dCartX -= 4;
    cartX -= 4;
  }
  drawBullets();
  drawPlatform();
  drawCart();
  drawBlocks();
  drawPoints();

  document.getElementById("fps").innerHTML = "fps:&nbsp" + calculateFPS();

  if (GAME_STATE == STATE.PLAYING) {
    animationID = requestAnimationFrame(draw);
  }
}

function drawPoints() {
  ctx.fillStyle = "#f00";
  ctx.font = `50px monospace`;
  const digitWidth = (Math.log10(points) + 1) * 50;
  ctx.fillText(points ? points : " 0 ", canvas.width - digitWidth, 100);
  ctx.closePath();
}

function drawBlocks() {
  for (j = 0; (polygon = polygonArray[j]); j++) {
    polygon.checkBoundingConditions();
    ctx.beginPath();
    polygon.drawRadius = Math.min(80, Math.max(polygon.size, minPolygonSize));
    ctx.moveTo(
      polygon.x + polygon.drawRadius * Math.cos(0),
      polygon.y + polygon.drawRadius * Math.sin(0)
    );
    for (var i = 1; i <= polygon.numberOfSides; i += 1) {
      ctx.lineTo(
        polygon.x +
          polygon.drawRadius *
            Math.cos((i * 2 * Math.PI) / polygon.numberOfSides),
        polygon.y +
          polygon.drawRadius *
            Math.sin((i * 2 * Math.PI) / polygon.numberOfSides)
      );
    }
    ctx.fillStyle = polygon.fillColor;
    ctx.fill();
    ctx.fillStyle = "#fff";
    ctx.font = `${polygon.drawRadius}px monospace`;
    const digitWidth = (Math.log10(polygon.size) + 1) * polygon.drawRadius;
    ctx.fillText(
      polygon.size,
      polygon.x - digitWidth / 4,
      polygon.y + polygon.drawRadius / 3
    );
    ctx.closePath();
    polygon.x += polygon.Vx;
    polygon.y += polygon.Vy;
  }
  nowPolygonInterval = performance.now() - lastPolygonTime;
  const interval = Math.max(
    2,
    Math.min(
      polygonMinInterval * (10 / Math.max(totalPolygonCount, 1)),
      polygonMinInterval
    )
  );
  if (nowPolygonInterval / 1000 >= interval) {
    lastPolygonTime = performance.now();
    totalPolygonCount++;
    polygonArray.push(new createPolygon());
    if (totalPolygonCount % 5 === 0) {
      maxPolygonSize *= 2;
    }
    console.log(polygonArray);
  }
}

class createPolygon {
  constructor() {
    this.x = polygonLeftDefault;
    this.y = polygonYDefault;
    this.size = Math.floor(
      minPolygonSize + Math.random() * (maxPolygonSize - minPolygonSize)
    );
    this.numberOfSides = Math.floor(
      minPolygonSide + Math.random() * (maxPolygonSide - minPolygonSide)
    );
    this.fillColor =
      polygonColors[Math.floor(Math.random() * polygonColors.length)];
    this.Vx = polygonDefaultVx;
    this.Vy = polygonDefaultVy;
    this.drawRadius = Math.min(80, Math.max(this.size, minPolygonSize));
  }
  checkBoundingConditions() {
    //set x boundaries
    if (this.x + this.drawRadius >= canvas.width) {
      this.x = canvas.width - this.drawRadius;
      this.Vx = -this.Vx;
    } else if (
      this.x - this.drawRadius <= 0 &&
      this.x - this.drawRadius > this.Vx
    ) {
      this.x = this.drawRadius;
      this.Vx = -this.Vx;
    }

    //set y boundaries
    if (
      this.x - this.drawRadius > this.Vx &&
      this.y + this.drawRadius < canvas.height - platformHeight
    ) {
      this.Vy += accelerationY;
    } else if (this.y + this.drawRadius >= canvas.height - platformHeight) {
      this.Vy = -this.Vy;
      this.Vy += accelerationY;
    }

    // check collission with bullets
    for (let i = 0; i < bulletArray.length; i++) {
      const bullet = bulletArray[i];
      const dx = bullet.x - this.x;
      const dy = bullet.y - this.y;
      const distance = Math.sqrt(dx * dx + dy * dy);

      if (distance < bulletRadius + this.drawRadius) {
        bulletArray.splice(i, 1); // bullet hit, remove it

        const pts = Math.min(5, this.size);
        this.size -= pts;
        points += pts;

        // remove block if negative size
        if (this.size <= 0) {
          const polygonIndex = polygonArray.indexOf(this);
          if (polygonIndex > -1) {
            polygonArray.splice(polygonIndex, 1);
          }
        }
        break;
      }
    }

    //check collision with cart
    if (
      this.y + this.drawRadius >=
      canvas.height - platformHeight - cartWheel1.height / 2
    ) {
      if (Math.abs(this.x - cartX) < cartBody.width + cartWheel1.width / 2) {
        endGame();
      }
    }
  }
}

function drawBullets() {
  for (i = 0; (bullet = bulletArray[i]); i++) {
    if (bullet.y < 0) {
      bulletArray.shift();
      continue;
    }
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.arc(bullet.x, bullet.y, bulletRadius, 0, Math.PI * 2, false);
    ctx.fillStyle = "#fff";
    ctx.strokeStyle = "#ff0000";
    ctx.fill();
    ctx.stroke();
    ctx.closePath();

    const time = (performance.now() - bullet.t) / 1000;
    if (time < 1) {
      bullet.vy -= 0.2;
    }
    bullet.y += bullet.vy;
  }
  nowBulletInterval = performance.now() - lastBulletTime;
  if (ctrlPressed && nowBulletInterval / 1000 >= bulletMinInterval) {
    lastBulletTime = performance.now();
    bulletArray.push(new makeABullet());
    //console.log("vedi vanne");
  }
}

class makeABullet {
  constructor() {
    this.x = cartX + bulletRadius + 6;
    this.y =
      canvas.height - platformHeight - cartBody.height - cartWheel2.height / 2;
    this.t = performance.now();
    this.vy = -4;
  }
}

function drawPlatform() {
  ctx.beginPath();
  ctx.rect(0, canvas.height - platformHeight, canvas.width, 100);
  ctx.fillStyle = "#B14800";
  ctx.fill();
  ctx.closePath();
}
function drawCart() {
  ctx.drawImage(
    cartBody,
    cartX,
    canvas.height - platformHeight - cartBody.height - cartWheel1.height / 2
  );
  ctx.save();
  ctx.translate(cartX, canvas.height - cartWheel1.height / 2 - platformHeight);
  ctx.rotate(dCartX / (4 * Math.PI)); // rotate
  ctx.drawImage(cartWheel1, -cartWheel1.width / 2, -cartWheel1.height / 2);
  ctx.restore();
  //ctx.rotate(-dCartX/(10*Math.PI)); // rotat
  //ctx.translate(-cartX, -canvas.height+cartWheel1.height/2+platformHeight);
  ctx.save();
  ctx.translate(
    cartX + cartBody.width,
    canvas.height - cartWheel2.height / 2 - platformHeight
  );
  ctx.rotate(dCartX / (4 * Math.PI)); // rotate
  ctx.drawImage(cartWheel2, -cartWheel2.width / 2, -cartWheel2.height / 2);
  ctx.restore();
}

function calculateFPS() {
  if (!lastCalledTime) {
    lastCalledTime = performance.now();
    return 0;
  }
  fps = Math.floor(1000 / (performance.now() - lastCalledTime));
  lastCalledTime = performance.now();
  return fps;
}

function endGame() {
  gameControls.style.display = "none";
  pauseControls.style.display = "none";
  endControls.style.display = "flex";
  cancelAnimationFrame(animationID);
  GAME_STATE = STATE.GAME_OVER;
}

function pauseGame() {
  gameControls.style.display = "none";
  pauseControls.style.display = "flex";
  cancelAnimationFrame(animationID);
  GAME_STATE = STATE.PAUSE;
}

function startGame() {
  if (GAME_STATE == STATE.MENU) {
    menuControls.style.display = "none";
  }
  if (GAME_STATE == STATE.PAUSE) {
    pauseControls.style.display = "none";
  }
  if (GAME_STATE == STATE.MENU || GAME_STATE == STATE.PAUSE) {
    initializeComponents();
    animationID = requestAnimationFrame(draw);
    GAME_STATE = STATE.PLAYING;
    gameControls.style.display = "flex";
  }
}
